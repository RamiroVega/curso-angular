import { DestinoViaje } from './detino-viaje.models';

export class DestinosApiClient {
    destinos: DestinoViaje[];
    constructor() {
       this.destinos = [];
    }

    add(d: DestinoViaje): void {
        this.destinos.push(d);
    }
    getAll() {
        return this.destinos;
    }
}
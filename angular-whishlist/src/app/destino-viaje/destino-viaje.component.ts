import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/detino-viaje.models';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
 @Input() destino: DestinoViaje;
 @HostBinding('attr.class') cssClass = 'col-md-4';
 @Output() clicked: EventEmitter<DestinoViaje>;
 @Input('idx') position: number;


  constructor() {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }
  ir(): boolean{
    this.clicked.emit(this.destino);
    return false;
  }

}

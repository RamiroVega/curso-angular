import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinosApiClient } from '../models/destinos-api-client.model';
import { DestinoViaje } from '../models/detino-viaje.models';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css']
})
export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;

  constructor(public destinosApiClient: DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje): void{
    this.destinosApiClient.add(d)
    this.onItemAdded.emit(d);
  }

  elegido(e: DestinoViaje){
    this.destinosApiClient.getAll().forEach((x) => {
        x.setSelected(false);
      });
    e.setSelected(true);
  }

}

